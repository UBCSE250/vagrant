#!/usr/bin/env bash

echo "Provisioning..."
date > /etc/vagrant_provisioned_at

apt-get update -y
apt-get upgrade -y

apt-get install -y ubuntu-desktop

apt-get install -y build-essential clang gdb valgrind
apt-get install -y snapd
snap install atom --classic

echo "done!"
